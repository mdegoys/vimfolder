" Pathogen initialization has to be before file type detection, according
" to :http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/
autocmd vimenter * NERDTree
execute pathogen#infect()

" Colors {{{
syntax on
colorscheme molokai
" }}}

" Spaces & Tabs {{{
set expandtab " On pressing tab, insert spaces instead of tabs
set tabstop=2 softtabstop=2 " show existing tab with 2 spaces width
set shiftwidth=2 " when indenting with '>', use 2 spaces width
filetype plugin indent on
set autoindent
" }}}

" UI Layout {{{
set wildmenu " visual autocomplete for command menu
set showmatch " highlight matching [{()}]
set number "Having line number displayed
set cursorline " highlight current line
" }}}

" Searching {{{
set path+=** " search down into subfolders
set ignorecase          " ignore case when searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" }}}

" Folding {{{
"=== folding ===
set foldmethod=indent   " fold based on indent level
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
nnoremap <space> za
set foldlevelstart=10   " start with fold level of 1
" }}}

" Leader Shortcuts {{{
let mapleader=","       " leader is comma
nnoremap <leader><space> :nohlsearch<CR> " turn off search highlight
nnoremap <leader>pry o binding.pry<ESC>
nnoremap <F5> :NERDTreeToggle<CR>
" }}}

"Whenever you select/yank something put it in the clipboard as well
set guioptions+=a
set clipboard=unnamed

" Pasted from https://gist.github.com/vallettea/f551d2a0721ca403285f : cancels
" compatibility with vi
set nocompatible

" CtrlP basic options
" https://github.com/ctrlpvim/ctrlp.vim
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" Read scripts contained in .vim/config
" runtime! config/**/*.vim

" FZF If installed using Homebrew
" set rtp+=/usr/local/opt/fzf
" set rtp+=~/.fzf

" Hard time :)
let g:hardtime_default_on = 1
let g:list_of_disabled_keys = ["<UP>", "<DOWN>", "<LEFT>", "<RIGHT>"]


" To automatically reload vim every time vimrc has been saved
" https://superuser.com/questions/132029/how-do-you-reload-your-vimrc-file-without-restarting-vim
if has ('autocmd') " Remain compatible with earlier versions
 augroup vimrc     " Source vim configuration upon save
    autocmd! BufWritePost $MYVIMRC source % | echom "Reloaded " . $MYVIMRC | redraw
    autocmd! BufWritePost $MYGVIMRC if has('gui_running') | so % | echom "Reloaded " . $MYGVIMRC | endif | redraw
  augroup END
endif " has autocmd

" vim:foldmethod=marker:foldlevel=0
packadd! matchit

" :help airline-tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#switch_buffers_and_tabs = 1
let g:airline#extensions#tabline#show_splits = 1
let g:airline#extensions#tabline#show_buffers = 1 " * enable/disable displaying buffers with a single tab. (c) >
set hidden "It hides buffers instead of closing them
nnoremap <C-N> :bnext<CR>
nnoremap <C-B> :bprev<CR>
